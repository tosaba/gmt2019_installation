---
theme: base-theme
marp: true
style: |
  @import url('https://fonts.googleapis.com/css?family=Roboto+Condensed');
  section {

    font-family: 'Roboto Condensed', sans-serif;
  }

---



<!--

_class: imc
footer: ![width:70px](https://bitbucket.org/tosaba/gmt2019_installation/raw/fb325c05cb6ed731074bf8c072e784ae8f3001ba/logo-gmt5.png)
_footer: ':arrow_right: gmt2019-installation.netlify.com'
-->

# <!-- fit--> Data Analysis in Geosciences '19
## GMT Installation



### Tobias Baumann
:envelope: baumann@uni-mainz.de




![bg right width:500px](https://bitbucket.org/tosaba/gmt2019_installation/raw/fb325c05cb6ed731074bf8c072e784ae8f3001ba/logo-gmt5.png)


---

# Information

- **Room** 	00 462 SR Bibliothek
- :calendar: 05/03 - 08/03 
- 9-17 Uhr :clock9: - :clock5:
  - **Tue** Linux intro / CLI & GMT basics
  - **Wed** GMT basics
  - **Thu** Maps, projections, filters, 
  - **Fri** Project work
- Do you have your own data (MSc-topic)?

---

# Examples


- Wikipedia maps
- https://de.wikipedia.org/wiki/Generic_Mapping_Tools
  
![width:500px](https://bitbucket.org/tosaba/gmt2019_installation/raw/fb325c05cb6ed731074bf8c072e784ae8f3001ba/MacedonEmpire.jpg)

![bg right contain](https://bitbucket.org/tosaba/gmt2019_installation/raw/fb325c05cb6ed731074bf8c072e784ae8f3001ba/Deutschland_topo.jpg)


---

# Examples
- Volumetric changes from laccolith emplacement
![bg right fit](https://bitbucket.org/tosaba/gmt2019_installation/raw/fb325c05cb6ed731074bf8c072e784ae8f3001ba/nature_CastroEtAl.png)

- J.Castro et al. (2016). _nature communications._



---

# Examples

- Illustrate Slopes from lidar data
- https://www.gadom.ski/2017/01/23/slope-maps-with-gmt.html

![bg fit right](https://bitbucket.org/tosaba/gmt2019_installation/raw/fb325c05cb6ed731074bf8c072e784ae8f3001ba/hidden-valley-slope-angle.png)

---

# Examples

- Bathymetry /ship tracs
   
![bg right fit](https://bitbucket.org/tosaba/gmt2019_installation/raw/fb325c05cb6ed731074bf8c072e784ae8f3001ba/janmayen.png)

---

# Installation

## Linux users

- Follow building instructions on
  [http://gmt.soest.hawaii.edu/projects/gmt/wiki/BuildingGMT](http://gmt.soest.hawaii.edu/projects/gmt/wiki/BuildingGMT)


---

# Installation

## MacOS users 

- Please install Macports/Fink
- Follow building instructions on
  [http://gmt.soest.hawaii.edu/projects/gmt/wiki/BuildingGMT](http://gmt.soest.hawaii.edu/projects/gmt/wiki/BuildingGMT)
  



---

# Installation

## Windows 10 users

- Install Ubuntu bash on win 10
- Please follow [https://www.windowscentral.com/how-install-bash-shell-command-line-windows-10](https://www.windowscentral.com/how-install-bash-shell-command-line-windows-10)
- When succeeded with bash integration, continue with regular Ubuntu build
- Install notepad++ to create scripts [https://notepad-plus-plus.org/](https://notepad-plus-plus.org/)


---

# Installation

## Windows <10 users

- Please follow the cygwin section on [http://gmt.soest.hawaii.edu/projects/gmt/wiki/BuildingGMT](http://gmt.soest.hawaii.edu/projects/gmt/wiki/BuildingGMT)




---

# Windows 10

## Create link to Windows user home directory


``` console
$ nano .bashrc
```
``` bash
# Link to windows home directory

export WINHOME=/mnt/c/Users/USER/
```

``` console
$ source .bashrc
```


---

# MacOS

## Install dependencies


``` console
$ sudo port install cmake gmake ghostscript netcdf git
```
``` console
$ sudo port install gdal +curl +geos +hdf5 +netcdf +openjpeg 
```

``` console
$ sudo port install GraphicsMagick
```

Now continue with **Building GMT from source**

---

# Debian/Ubuntu

## 1) Update and upgrade your system


``` console
$ sudo apt update
```

``` console
$ sudo apt upgrade
```


---

# Debian/Ubuntu

## 2) Install dependencies


``` console
$ sudo apt install build-essential cmake libcurl4-gnutls-dev libnetcdf-dev libgdal-dev libfftw3-dev libpcre3-dev liblapack-dev libblas-dev graphicsmagick git
```


Now continue with **Building GMT from source**

---


# Building GMT from source (**on any system**)
### Download DCW and GSHHG data

![](https://bitbucket.org/tosaba/gmt2019_installation/raw/5e9c1898d7f8f199dcbd797e926ccfc65615b9cc/dcw_gshhg.png)

---


# Building GMT from source (**on any system**)

 Usually, the files are in `~/Downloads`

``` console
$ cd ~/Downloads # Win10 users: cd ${WINHOME}/Downloads
```
``` console
$ tar -xf dcw-gmt-1.1.4.tar.gz
```
``` console
$ tar -xf gshhg-gmt-2.3.7.tar.gz
```
Get download path & return to gmt directory

``` console
$ pwd
/YOUR/CURRENT/PATH/  # COPY this path you need it later
```
``` console
$  cd 
```

---

# Building GMT from source (**on any system**)

## 1) Get gmt and configure cmake-file

``` console
$ cd ; mkdir software; cd software
```

``` console
$ git clone https://github.com/GenericMappingTools/gmt.git
```

``` console
$ cd gmt
```

``` console
$ cp cmake/ConfigUserTemplate.cmake cmake/ConfigUser.cmake
```

``` console
$ nano cmake/ConfigUser.cmake
```

---

# Building GMT from source (**on any system**)
### Edit ConfigUser.cmake (Remove '#' of relevant lines and include paths)

``` bash
# Installation path (usually defaults to /usr/local) [auto]:
set (CMAKE_INSTALL_PREFIX "/opt/gmt")

...

# Set path to GSHHG Shoreline Database [auto]:
set (GSHHG_ROOT "/home/YOUR/PATH/gmt/gshhg-gmt-2.3.7")

# Copy GSHHG files to $/coast [FALSE]:
set (COPY_GSHHG TRUE)

# Set path to DCW Digital Chart of the World for GMT [auto]:
set (DCW_ROOT "/home/YOUR/PATH/gmt/dcw-gmt-1.1.4")

# Copy DCW files to $/dcw [FALSE]:
set (COPY_DCW TRUE)
```


---

# Building GMT from source (**on any system**)

## 2) Make and make install

``` console
$ mkdir build
```

``` console
$ cd build
```

``` console
$ cmake ..
```

``` console
$ make
```

``` console
$ sudo make install
```


---

# Building GMT from source (**on any system**)

## 3) Update environment variable **PATH**

``` console
$ cd
```

``` console
$ nano .bashrc
```

### ON MAC:
``` console
$ nano .profile
```

---
# Building GMT from source (**on any system**)

### Edit .bashrc/.profile with nano

``` bash

# GMT installation
export PATH=/opt/gmt/bin:${PATH}

```


``` console
$ source .bashrc
```


---

# Test the GMT installation

``` console
$ cd
```
``` console
$ pscoast -R20/30/50/45r -JT35/25 -Bag -Df -Na -Wthin -Gseashell -Slightblue > MeinErsterGMTPlot.ps
```
``` console
$ psconvert -Tf MeinErsterGMTPlot.ps
```


--- 

# Test the GMT installation

Now you have a file called `MeinErsterGMTPlot.pdf` in your `home` directory that should look like this one.

![bg right fit](https://bitbucket.org/tosaba/gmt2019_installation/raw/5e9c1898d7f8f199dcbd797e926ccfc65615b9cc/test_gmt.png)
